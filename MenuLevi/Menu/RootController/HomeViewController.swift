//
//  ViewController.swift
//  MenuLevi
//
//  Created by Sattin on 28/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {
    
    // Connection Objects
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // Declarations
    lazy var menuBar: MenuBar = {
        let mb = MenuBar()
        mb.backgroundColor = .lightGray
        mb.layer.cornerRadius = 4
        mb.homeController = self
        return mb
    }()
    let titles: Array<String> = Menu.shared.items.map({ return $0.name! })
    
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        configureView()
        setupCollectionView()
        setupMenuBar()
        refresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        registerListeners()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        removeListeners()
    }
    
    // MARK: Arrange View
    func configureView() {
    
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func refresh() {
    
        collectionView.reloadData()
    }
    
    
    func registerListeners() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(rotated(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func removeListeners() {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func rotated(_ notification: Notification) {
        
        if UIDevice.current.orientation.isLandscape {
            
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
        else {
            
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
    }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func setupCollectionView() {
        
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        
        collectionView?.contentInset = UIEdgeInsetsMake(58, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(58, 0, 0, 0)
        collectionView?.isPagingEnabled = true
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CommonCell")
    }
    
    
    fileprivate func setupMenuBar() {
        
        view.addSubview(menuBar)
        view.addConstraintsWithFormat("H:|-8-[v0]-8-|", views: menuBar)
        view.addConstraintsWithFormat("V:[v0(50)]", views: menuBar)
        menuBar.topAnchor.constraint(equalTo: collectionView.topAnchor).isActive = true
    }
    
    func scrollToMenuIndex(_ menuIndex: Int) {
        
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(), animated: true)
        setTitleForIndex(menuIndex)
    }
    
    fileprivate func setTitleForIndex(_ index: Int) {
        
        navigationItem.title = titles[index]
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / CGFloat(titles.count)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let index = targetContentOffset.pointee.x / view.frame.width
        let indexPath = IndexPath(item: Int(index), section: 0)
        
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition())
        setTitleForIndex(Int(index))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // Titles always same as menu count
        return titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Data Cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommonCell", for: indexPath)
        cell.contentView.subviews.forEach({ $0.removeFromSuperview() })
        cell.contentView.addSubview(Menu.shared.items[indexPath.item].controller!.view)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
 
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height - 50)
    }
}


