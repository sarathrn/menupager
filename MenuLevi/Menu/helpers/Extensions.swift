//
//  UIView.swift
//  MenuLevi
//
//  Created by Sattin on 28/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    // Add constraint
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
    
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
        
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}


extension String {
    
    // Calculate Content Width
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return ceil(boundingBox.width)
    }
}


