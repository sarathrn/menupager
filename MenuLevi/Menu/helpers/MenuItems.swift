//
//  MenuItems.swift
//  MenuLevi
//
//  Created by Sattin on 29/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import UIKit

class Menu: NSObject {
    
    // Declarations
    static let shared = Menu()
    var items = Array<MenuItem>()
    
    // MARK: Life Cycle
    override init() {
        super.init()
        
        load()
    }
    
    
    private func load() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let favController = storyboard.instantiateViewController(withIdentifier: "FavouriteViewController") as! FavouriteViewController
        
        let menuSource = [["name": "Favourites", "controller": favController],
                          ["name": "Search History", "controller": UIViewController()],
                          ["name": "Recent Chats", "controller": UIViewController()],
                          ["name": "Appointments", "controller": UIViewController()]
        ]
        items = menuSource.map({ return MenuItem($0) })
    }
}

class MenuItem: NSObject {
    
    var name: String?
    var controller: UIViewController?
    
    override init() {
        super.init()
    }
    
    init(_ value: Dictionary<String, Any>) {
        
        super.init()
        self.name = value["name"] as? String
        self.controller = value["controller"] as? UIViewController
    }
}
