//
//  MenuBarCell.swift
//  MenuLevi
//
//  Created by Sattin on 29/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import UIKit

class BaseCell: UICollectionViewCell {
   
    // MARK: Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
    }
}


class MenuCell: BaseCell {
    
    // MARK: Connection Objects
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        return label
    }()
    
    
    // Declarations
    static var identifier: String {
        return String(describing: self)
    }
    
    override var isHighlighted: Bool {
        didSet {
            titleLabel.textColor = isHighlighted ? UIColor.blue : UIColor.black
        }
    }
    
    override var isSelected: Bool {
        didSet {
            titleLabel.textColor = isSelected ? UIColor.blue : UIColor.black
        }
    }
    
    // Set Data From Parent
    var title: String? {
        didSet {
            
            titleLabel.text = title ?? nil
        }
    }
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(titleLabel)
        addConstraintsWithFormat("H:|[v0]|", views: titleLabel)
        addConstraintsWithFormat("V:|[v0]|", views: titleLabel)
    }
    
}

