//
//  MenuBarView.swift
//  MenuLevi
//
//  Created by Sattin on 28/12/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import Foundation
import UIKit


class MenuBar: UIView {
    
    // Declarations
    lazy var collectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .white
        cv.layer.cornerRadius = 4
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    weak var homeController: HomeViewController?
    let menuSource = Menu.shared.items
    var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
    
    
    // MARK: Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
       initiate()
        registerListeners()
    }
    
    deinit {
        
        removeListeners()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    
    // MARK: Arrane View
    func initiate() {
        
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: MenuCell.identifier)
        addSubview(collectionView)
    
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        
        selectMenuAt(0)
        setupHorizontalBar()
    }
    
    func registerListeners() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(rotated(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func removeListeners() {
        
         NotificationCenter.default.removeObserver(self)
    }
    
    @objc func rotated(_ notification: Notification) {
        
        if UIDevice.current.orientation.isLandscape {
        
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
        else {
        
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    func selectMenuAt(_ index: Int) {
        
        let selectedIndexPath = IndexPath(item: index, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
    }
    
    func setupHorizontalBar() {
        
        let horizontalBarView = UIImageView()
        horizontalBarView.image = UIImage(named: "down_arrow")
        horizontalBarView.contentMode = .scaleAspectFit
        horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(horizontalBarView)
        
        horizontalBarLeftAnchorConstraint = horizontalBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
        horizontalBarLeftAnchorConstraint?.isActive = true
        
        horizontalBarView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 10).isActive = true
        horizontalBarView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/4).isActive = true
        horizontalBarView.heightAnchor.constraint(equalToConstant: 10).isActive = true
    }
}


extension MenuBar: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        homeController?.scrollToMenuIndex(indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return menuSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCell.identifier, for: indexPath) as! MenuCell
        cell.title = menuSource[indexPath.item].name
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        /*  Width  = Label Content Width
            Height = Collection Height
         */
        
        /*
        // Get String
        let title = menuSource[indexPath.item].name
        let height = collectionView.frame.height
       
        var itemWidth = title!.width(withConstrainedHeight: height, font: UIFont.systemFont(ofSize: 14))
        let screenMaxWidth = collectionView.frame.width / CGFloat(menuSource.count)
        if itemWidth < screenMaxWidth {
            itemWidth = screenMaxWidth
        }
        */
        
        let margins = (menuSource.count * 2)
        let width = (frame.width / CGFloat(menuSource.count)) - CGFloat(margins)
        return CGSize(width: width , height: frame.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 2
    }
}
